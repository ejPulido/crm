import { useNavigate,Form, useActionData, redirect } from "react-router-dom"
import Formulario from "../components/Formulario"
import Error from "../components/Error"
import { AddClient } from "../data/ClientData"

export async function action({request}){
    const formData = await request.formData()
    const data = Object.fromEntries(formData)
    let regex = new RegExp("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])");
    const email = formData.get('email')
    const errors = []

    //validacion

    if(Object.values(data).includes('')){
        errors.push('Todos los campos son obligatorios')
    }
    if(!regex.test(email)){
        errors.push('email no valido')
    }
    if(errors.length){
        console.log('si hay errores', errors);
        return errors
    }
    
    await AddClient(data)
    
    return redirect('/')
}


function Newclient (){
    const navigate = useNavigate()
    const errors = useActionData()

    console.log('errors en componentes', errors);
    return (
        <>
        
        


        <h1 className="font-black text-4xl text-blue-900">Nuevo Cliente</h1>
        <p className="mt-3">Llena todos los campos para registrar un nuevo cliente</p>
        
        <div className="flex justify-end">
            <button className="bg-blue-800 hover:bg-blue-900 text-white px-3 py-1 font-bold uppercase"
            onClick={()=>navigate(-1)}>Volver</button>
        </div>

            <div className="bg-white shadow rounded-md md:w-3/4 mx-auto px-5 py-10 mt-20">
                <Form 
                    method="POST"
                    action=""
                    noValidate
                >
                    {errors && errors.map((error,i) =>{
            return <Error key = {i}>{error}</Error>
        })}
                <Formulario/>
                <input type="submit" className="cursor-pointer mt-5 w-full bg-blue-800 p-3 uppercase font-bold text-white text-lg" 
                value="Registrar Cliente"
                />
                </Form>
            </div>
        </>
    )
}

export default Newclient