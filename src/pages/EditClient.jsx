import { Form, useNavigate, useLoaderData, redirect, useActionData } from "react-router-dom";
import { GetClientData, UpdateClient } from "../data/ClientData";
import Formulario from "../components/Formulario";
import Error from "../components/Error";

export async function loader({params}){
    const client = await GetClientData(params.idClient)
    if(!Object.values(client).length){
        throw new Response('',{
            status:404,
            statusText:'No hay resultados'
        })
    }
    console.log(client);
    return client
}

export async function action({request, params}){
    const formData = await request.formData()
    const data = Object.fromEntries(formData)
    let regex = new RegExp("([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+\")@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])");
    const email = formData.get('email')
    const errors = []
    console.log(params, request);
    //validacion
    
    if(Object.values(data).includes('')){
        errors.push('Todos los campos son obligatorios')
    }
    if(!regex.test(email)){
        errors.push('email no valido')
    }
    if(errors.length){
        console.log('si hay errores', errors);
        return errors
    }
    
    await UpdateClient(params.idClient,data)
    
    return redirect('/')
    console.log('action de edicion');
    return null
}



export function EditClient (){
    const navigate = useNavigate()
    const client = useLoaderData()
    const errors = useActionData()


  
    return (
        <>
        
        <h1 className="font-black text-4xl text-blue-900">Editar Cliente</h1>
        <p className="mt-3">A continuación podrás modificar los datos de un cliente</p>
        
        <div className="flex justify-end">
            <button className="bg-blue-800 hover:bg-blue-900 text-white px-3 py-1 font-bold uppercase"
            onClick={()=>navigate(-1)}>Volver</button>
        </div>

            <div className="bg-white shadow rounded-md md:w-3/4 mx-auto px-5 py-10 mt-20">
                <Form 
                    method="POST"
                    action=""
                    noValidate
                >
                  {errors && errors.map((error,i) =>{
            return <Error key = {i}>{error}</Error>
        })}
                <Formulario
                    cliente={client}
                />
                <input type="submit" className="cursor-pointer mt-5 w-full bg-blue-800 p-3 uppercase font-bold text-white text-lg" 
                value="Guardar"
                />
                </Form>
            </div>
        </>
    )
}

export default EditClient