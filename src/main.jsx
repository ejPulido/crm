import React from 'react'
import ReactDOM from 'react-dom/client'
import {createBrowserRouter, RouterProvider} from 'react-router-dom'
import Layout from './components/Layout'
import Index, {loader as loaderIndex} from './pages/Index'
import Newclient, {action as actionClient} from './pages/NewClient'
import ErrorPage from './components/ErrorPage'
import EditClient, {  loader as loaderEdit, action as actionEdit } from './pages/EditClient'
import {action as actionRemove} from './components/Cliente'
import './index.css'

const router = createBrowserRouter([
  {
    path:'/',
    element:<Layout/>, //componente plantilla en el que en todos los hijos
    children:[ //hijos del principal
        //todos los hijos van a tener el layout y cada hijo es una ruta
       {
        index: true,
        element:<Index/>,
        loader:loaderIndex,
        errorElement:<ErrorPage/>

       },
       
        {
          path:'/client/new',
          element: <Newclient/>,
          action:actionClient,
          errorElement:<ErrorPage/>
        },
        //ruta diamica para edicion
        {
          //lo que esta despues de ':' es una variable dinamica que se va a crear
          path:'/client/:idClient/edit',
          element:<EditClient/>,
          loader:loaderEdit,
          action:actionEdit,
          errorElement:<ErrorPage/>
        },
        {
          path:'/client/:idClient/remove',
          action:actionRemove
        }

    ]
  }

])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
   <RouterProvider router={router}/>
  </React.StrictMode>,
)
