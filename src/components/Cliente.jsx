import { Form, useNavigate, redirect } from "react-router-dom"
import { RemoveClient } from "../data/ClientData";

export async function action ({params}){
    console.log('desde el action para eliminar', params.idClient);
    await RemoveClient(params.idClient)
    
    return redirect('/')
}


function Cliente ({cliente}){
    const navigate = useNavigate()
    const {nombre, empresa, telefono, email, id} = cliente
    return(<> 
        <tr className="border-b">
            <td className="p-6">
                <p className="text-2xl text-gray-800">{nombre}</p>
                <p>{empresa}</p>
            </td>
            <td className="p-6">
                <p className="text-gray-600"><span className="text-gray-800 uppercase font-bold">Email: </span>{email}</p>
                <p className="text-gray-600"><span className="text-gray-800 uppercase font-bold">Telefono: </span>{telefono}</p>
            </td>

            <td className="p-6 flex gap-3">
                <button
                    onClick={()=> {navigate(`/client/${id}/edit`)}}
                    type="button"
                    className="text-blue-600 hover:text-blue-700 uppercase font-bold text-xs"
                >Editar</button>
                <Form
                method="post"
                action={`/client/${id}/remove`}
                onSubmit = {(e)=>{
                    if(!confirm('¿Desea eliminar este cliente?')){
                        e.preventDefault()
                    }
                }}
                >
                <button
                    type="submit"
                    className="text-red-600 hover:text-red-700 uppercase font-bold text-xs">Eliminar</button>

                </Form>
                     
            </td>
        </tr>

    </>)
}

export default Cliente