import { Result } from "postcss";

export async function ClientData(){
    const url = import.meta.env.VITE_API_URL
    const response = await fetch(url)
    const data = await response.json()
 
    return data
}



export async function GetClientData(id){
    const url = import.meta.env.VITE_API_URL
    const response = await fetch(`${url}/${id}`)
    const data = await response.json()
 
    return data
}

export async function RemoveClient(id){
    try{

        const response = await fetch(`${import.meta.env.VITE_API_URL}/${id}`, {
            method:'DELETE',
      
        })
       return await response.json()
    }catch(e){

    }
}

export async function AddClient(data){
    try{

        const response = await fetch(import.meta.env.VITE_API_URL, {
            method:'POST',
            body: JSON.stringify(data),
            headers:{
                "Content-Type": "application/json"
            }
        })
       return await response.json()
    }catch(e){

    }
}

export async function UpdateClient(id,data){
    console.log('Estamos haciendo update');
    try{

        const response = await fetch(`${import.meta.env.VITE_API_URL}/${id}`, {
            method:'PUT',
            body: JSON.stringify(data),
            headers:{
                "Content-Type": "application/json"
            }
        })
       return await response.json()
    }catch(e){

    }
}